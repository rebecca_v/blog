# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBlog::Application.config.secret_key_base = '1b7b70de3cafcf23d4df77f4d828d5937f5b29c709cdef3ea878fc61ac227cebe040973d404d20f3bc5bca1ef324755e4295e1ccca2e11b15e2e1fb422352d17'
